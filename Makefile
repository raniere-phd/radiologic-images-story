ALL_RMD := $(wildcard *.Rmd)

all: html html_book

html: article.html

html_book: _book

pdf: article.pdf

_book: $(ALL_RMD) references-from-zotero.bib
	Rscript -e 'bookdown::render_book(".", "bookdown::gitbook")'

article.html: $(ALL_RMD) references-from-zotero.bib
	Rscript -e 'bookdown::render_book(".", "bookdown::html_document2")'

article.pdf: $(ALL_RMD) references-from-zotero.bib
	Rscript -e 'bookdown::render_book(".", "bookdown::pdf_book")' && mv _book/article.pdf $@

references-from-zotero.bib:
	@echo 'Use Better BibTeX for Zotero <https://retorque.re/zotero-better-bibtex> to keep keys updated.'
